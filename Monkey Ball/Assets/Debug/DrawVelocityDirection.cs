﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawVelocityDirection : MonoBehaviour
{


    void FixedUpdate()
    {
        try
        {
            Debug.DrawRay(transform.position, GetComponent<Rigidbody>().velocity, Color.red, 0.0000001f, false);
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward), Color.yellow, 0.000001f, false);
        }
        catch(MissingComponentException playerRigidBodyDestroyed) { }
    }
}
