﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public struct DailyData
{
    public string dayPlayed;
    public int timesOpened;
    public List<GameData> games;
}

public struct GameData
{
    public LevelData[] levels;
    public LevelData totalLevelData;
}

public struct LevelData
{
    public bool played;
    public bool completed;
    public int fallouts;
    public int timeouts;
    public int resets;
    public int completionTime;
    public int bananasCollected;
    public int score;
}

public struct ReadData
{
    public List<string[]> dataHeaderList;
    public List<string[]> dataList;
    public string[] headerArray;
    public string[] dataArray;
}

public class Analytics : MonoBehaviour 
{
    public List<DailyData> dailyData;

    #region Creation Functions
    public DailyData CreateDailyData()
    {
        DailyData newDay = new DailyData();

        newDay.dayPlayed = DateTime.Today.ToString();
        newDay.timesOpened = 1;
        newDay.games = new List<GameData>();

        return newDay;
    }

    public DailyData CreateDailyData(string day, int timesOpened, List<GameData> games)
    {
        DailyData newDay = new DailyData();

        newDay.dayPlayed = day;
        newDay.timesOpened = timesOpened;
        newDay.games = games;

        return newDay;
    }

    public GameData CreateGameData()
    {
        GameData newGame = new GameData();

        newGame.levels = new LevelData[3];
        newGame.totalLevelData = new LevelData();

        return newGame;
    }

    public GameData CreateGameData(LevelData[] levelData, LevelData gameData)
    {
        GameData newGame = new GameData();

        newGame.levels = levelData;
        newGame.totalLevelData = gameData;

        return newGame;
    }

    public LevelData CreateLevelData()
    {
        LevelData newLevel = new LevelData();

        newLevel.played = false;
        newLevel.completed = false;
        newLevel.fallouts = 0;
        newLevel.timeouts = 0;
        newLevel.resets = 0;
        newLevel.completionTime = 0;
        newLevel.bananasCollected = 0;
        newLevel.score = 0;

        return newLevel;
    }

    public LevelData CreateLevelData(bool played, bool completed, int fallouts, int timeouts, int resets, int completionTime, int bananas,  int score)
    {
        LevelData newLevel = new LevelData();

        newLevel.played = played;
        newLevel.completed = completed;
        newLevel.fallouts = fallouts;
        newLevel.timeouts = timeouts;
        newLevel.resets = resets;
        newLevel.completionTime = completionTime;
        newLevel.bananasCollected = bananas;
        newLevel.score = score;

        return newLevel;
    }
    #endregion

    void Awake()
    {
        dailyData = new List<DailyData>();
        
    }

    public void InitializeDailyDataList()
    {
        if(File.Exists(Application.persistentDataPath + "/analytics.txt"))
        {
            ReadAnalyticsFile();
        }
        else
        {
            dailyData.Add(CreateDailyData());
        }
    }

    public void PullDirectorData()
    {
        Director directorData = GameObject.Find("Director").GetComponent<Director>();
        GameData directorGame = CreateGameData(directorData.levelData, directorData.gameData);

        if (dailyData.Count > 1)
            dailyData[dailyData.Count - 1].games.Add(directorGame);
        else
        {
            dailyData[0].games.Add(directorGame);
        }
    }

    #region File I/O
    void ReadAnalyticsFile()
    {
        StreamReader readAnalytics = new StreamReader(Application.persistentDataPath + "/analytics.txt");
        ReadData readInData = new ReadData();
        string readFileData;
        DateTime testDate;

        readInData.dataHeaderList = new List<string[]>();
        readInData.dataList = new List<string[]>();

        readFileData = readAnalytics.ReadLine();
        Debug.Log(readFileData);

        while (readFileData != null)
        {
            if(DateTime.TryParse(readFileData, out testDate))
            {
                readInData.headerArray = new string[2];
                readInData.headerArray[0] = readFileData;
                Debug.Log(readInData.headerArray[0]);
                readInData.headerArray[1] = readAnalytics.ReadLine();
                Debug.Log(readInData.headerArray[1]);
                readInData.dataHeaderList.Add(readInData.headerArray);
            }
            else
            {
                readInData.dataArray = new string[9];
                readInData.dataArray[0] = readFileData;
                Debug.Log(readInData.dataArray[0]);
                for(int i = 1; i < 9; i++)
                {
                    readInData.dataArray[i] = readAnalytics.ReadLine();
                    Debug.Log(readInData.dataArray[i]);
                }
                readInData.dataList.Add(readInData.dataArray);
            }
        }        
    }

    public void WriteAnalyticsFile()
    {
        StreamWriter writeAnalytics = new StreamWriter(Application.persistentDataPath + "/analytics.txt");
        for (int i = 0; i < dailyData.Count; i++)
        {
            // Write the day and times opened to the file
            writeAnalytics.WriteLine(dailyData[i].dayPlayed);
            writeAnalytics.WriteLine(dailyData[i].timesOpened);
            // Write each day to the file
            for (int j = 0; j < dailyData[i].games.Count; i++)
            {
                // Write each game for the day to the file
                writeAnalytics.WriteLine("Game " + (j + 1));
                for (int k = 0; k < 3; k++)
                {
                    // Write each level to the file
                    writeAnalytics.WriteLine("Level " + (k + 1));
                    writeAnalytics.WriteLine(dailyData[i].games[j].levels[k].played + "," +
                                             dailyData[i].games[j].levels[k].completed + "," +
                                             dailyData[i].games[j].levels[k].fallouts + "," +
                                             dailyData[i].games[j].levels[k].timeouts + "," +
                                             dailyData[i].games[j].levels[k].resets + "," +
                                             dailyData[i].games[j].levels[k].completionTime + "," +
                                             dailyData[i].games[j].levels[k].bananasCollected + "," +
                                             dailyData[i].games[j].levels[k].score);
                }
                // Write this game's totals to the file
                writeAnalytics.WriteLine("Game Totals");
                writeAnalytics.WriteLine(dailyData[i].games[j].totalLevelData.played + "," +
                                         dailyData[i].games[j].totalLevelData.completed + "," +
                                         dailyData[i].games[j].totalLevelData.fallouts + "," +
                                         dailyData[i].games[j].totalLevelData.timeouts + "," +
                                         dailyData[i].games[j].totalLevelData.resets + "," +
                                         dailyData[i].games[j].totalLevelData.completionTime + "," +
                                         dailyData[i].games[j].totalLevelData.bananasCollected + "," +
                                         dailyData[i].games[j].totalLevelData.score);
            }
        }
    }
    #endregion

}
