﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Director : MonoBehaviour
{
    public int bananaMultiplier;
    public int timeMultiplier;
    public Text gameOverTextbox;
    public GameObject banana;

    // Level data, stored in the level data array for each level
    public int levelStartTime;
    public int elapsedLevelTime;
    public int levelEndTime;
    public int fallouts;
    public int timeouts;
    public int resets;
    public int bananasCollected;
    public int score;

    // Data for each level
    public LevelData[] levelData;
    
    // Level data totals
    public LevelData gameData;
    
    // List of bananas in the level
    public GameObject[] bananas;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        levelData = new LevelData[3];
        StartGame();        
    }

    void Start()
    {
        Debug.Log(Application.persistentDataPath);
    }
    
    void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    void OnLevelWasLoaded()
    {
        if (SceneManager.GetActiveScene().buildIndex > 1)
        {
            if(levelData[SceneManager.GetActiveScene().buildIndex - 2].played == false)
                bananas = GameObject.FindGameObjectsWithTag("banana");            
        }
    }

    public void StartLevel()
    {
        levelData[SceneManager.GetActiveScene().buildIndex - 2].played = true;
        levelStartTime = GameObject.Find("Main Camera").GetComponent<Timer>().stageTime;
        fallouts = 0;
        timeouts = 0;
        resets = 0;
    }


    public void LevelComplete()
    {
        levelData[SceneManager.GetActiveScene().buildIndex - 2].completed = true;
        bananasCollected = int.Parse(GameObject.Find("CountText").GetComponent<Text>().text);
        levelEndTime = int.Parse(GameObject.Find("TimeText").GetComponent<Text>().text);
        elapsedLevelTime = levelStartTime - levelEndTime;        
        score = CalculateScore();
        StoreLevelData(SceneManager.GetActiveScene().buildIndex - 2);        
    }    

    public void GameComplete()
    {
        for (int i = 0; i < levelData.Length; i++)
        {
            if (levelData[i].completed)
            {
                gameData.fallouts += levelData[i].fallouts;
                gameData.timeouts += levelData[i].timeouts;
                gameData.resets += levelData[i].resets;
                gameData.completionTime += levelData[i].completionTime;
                gameData.bananasCollected += levelData[i].bananasCollected;
                gameData.score += levelData[i].score;
            }
        }
        //PushGameData();
    }

    public void SetGameOverText()
    {
        gameOverTextbox.text = "Fallouts:\t" + gameData.fallouts + "\n" +
                               "Timeouts:\t" + gameData.timeouts + "\n" +
                               "Resets:\t" + gameData.resets + "\n" +
                               "Total time:\t" + gameData.completionTime + "\n" +
                               "Bananas collected:\t" + gameData.bananasCollected + "\n" +
                               "Final Score:\t" + gameData.score;
    }

    void StoreLevelData(int index)
    {
        levelData[index].fallouts = fallouts;
        levelData[index].timeouts = timeouts;
        levelData[index].resets = resets;
        levelData[index].completionTime = elapsedLevelTime;
        levelData[index].bananasCollected = bananasCollected;
        levelData[index].score = score;
    }

    void PushGameData()
    {
        GetComponent<Analytics>().PullDirectorData();
    }

    public void Fallout()
    {
        fallouts++;
        Invoke("ResetBananas", 1.59f);
    }

    public void Timeout()
    {
        timeouts++;
        Invoke("ResetBananas", 1.59f);
    }

    public void Reset()
    {
        resets++;
        Invoke("ResetBananas", 1.59f);
    }

    void ResetBananas()
    {
        foreach (GameObject banana in bananas)
        {
            banana.SetActive(true);
        }
    }

    int CalculateScore()
    {
        return (bananasCollected * bananaMultiplier) + (levelEndTime * timeMultiplier) - (elapsedLevelTime * timeMultiplier);
    }
}
