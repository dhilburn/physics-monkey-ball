﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CatchPlayer : MonoBehaviour
{
    public GameObject water;
    public AudioClip deathNoise;

    Vector3 initialPlayerPos;
    Vector3 initialCameraPos;

    bool fallOut;

    void Start()
    {
        initialPlayerPos = GameObject.Find("Player").transform.position;
        initialCameraPos = GameObject.Find("Main Camera").transform.position;
        fallOut = false;
    }

    void Update()
    {
        if (transform.position.y < (water.transform.position.y - 1))
        {            
            GameObject.Find("Main Camera").GetComponent<AudioSource>().PlayOneShot(deathNoise);
            GameObject.Find("Main Camera").GetComponent<Timer>().StopTimer();
            GameObject.Find("Director").GetComponent<Director>().Fallout();
            Restart();
        }
    }

    void Restart()
    {
        GameObject.Find("Player").transform.position = initialPlayerPos;
        GameObject.Find("Main Camera").transform.position = initialCameraPos;
        GameObject.Find("Main Camera").GetComponent<Timer>().ResetTimer();
        GameObject.Find("CountText").GetComponent<Text>().text = "0";
    }
}
