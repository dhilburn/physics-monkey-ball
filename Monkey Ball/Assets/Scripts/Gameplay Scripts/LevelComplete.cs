﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelComplete : MonoBehaviour
{
    public GameObject levelComplete;
    public GameObject player;
    public AudioSource winNoise;

    
    public GameObject mainCanvas;
    public GameObject gameOverCanvas;

    GameObject director;

    void Start()
    {
        director = GameObject.Find("Director");
    }

    private void OnTriggerEnter(Collider other)
    {
        winNoise.Play();
        levelComplete.SetActive(true);
        GameObject.Find("Game Canvas").GetComponent<GameMenu>().HidePause();
        GameObject.Find("Main Camera").GetComponent<Timer>().StopTimer();

        director.GetComponent<Director>().LevelComplete();

        if (SceneManager.GetActiveScene().buildIndex == SceneManager.sceneCountInBuildSettings - 1)
        {
            director.GetComponent<Director>().GameComplete();
            Invoke("GameOver", 2.0f);
        }
        else
        {            
            Invoke("LoadNextScene", 2.0f);
        }
        //GameObject alert = Instantiate(levelComplete, new Vector3(0.5f, 0.5f, 0f), transform.rotation) as GameObject;
        Destroy(player.GetComponent<Rigidbody>());
    }

    void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    void GameOver()
    {
        mainCanvas.SetActive(false);
        gameOverCanvas.SetActive(true);
        director.GetComponent<Director>().gameOverTextbox = GameObject.Find("Stats").GetComponent<Text>();
        director.GetComponent<Director>().SetGameOverText();
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }
}
