﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoseGame : MonoBehaviour
{
    Vector3 initialPlayerPos;
    Vector3 initialCameraPos;
    public AudioClip deathNoise;

    void Start()
    {
        initialPlayerPos = GameObject.Find("Player").transform.position;
        initialCameraPos = GameObject.Find("Main Camera").transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {        
        GameObject.Find("Main Camera").GetComponent<AudioSource>().PlayOneShot(deathNoise);        
        GameObject.Find("Main Camera").GetComponent<Timer>().StopTimer();
        GameObject.Find("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        GameObject.Find("Director").GetComponent<Director>().Fallout();
        Invoke("Reload", 1.59f);
    }

    void Reload()
    {
        GameObject.Find("Player").transform.position = initialPlayerPos;
        GameObject.Find("Main Camera").transform.position = initialCameraPos;
        GameObject.Find("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        GameObject.Find("Main Camera").GetComponent<Timer>().ResetTimer();
        GameObject.Find("CountText").GetComponent<Text>().text = "0";
    }
}
