﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickupBananas : MonoBehaviour
{
    public AudioClip bananaPickup;
    public Text bananaText;

    void Start()
    {
        bananaText = GameObject.Find("CountText").GetComponent<Text>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("Player"))
        {
            AudioSource.PlayClipAtPoint(bananaPickup, transform.position);
            
            // Hacky score display/count
            // Parse current score from the text display and add 1
            int score = int.Parse(bananaText.text) + 1;
            bananaText.text = score.ToString();
            gameObject.SetActive(false);
        }
    }
}
