﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCam : MonoBehaviour
{
    public GameObject player;
        
    Vector3 playerOffset;
    Vector3 velocityDirection;

    Vector3 lookDirection;
    Quaternion lookRotation;

    GameObject director;

	// Use this for initialization
	void Start ()
    {
        playerOffset = transform.position - player.transform.position;
        director = GameObject.Find("Director");        
        director.GetComponent<Director>().StartLevel();
        DontDestroyOnLoad(director);        
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        transform.LookAt(player.transform.position);
        transform.position = player.transform.position + playerOffset;
        director.transform.position = transform.position;
	}
}
