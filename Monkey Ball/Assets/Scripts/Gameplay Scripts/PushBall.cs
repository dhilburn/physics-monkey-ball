﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushBall : MonoBehaviour {

    public Rigidbody playerRigidBody;
    public float forwardSpeed = 3;
    public Vector3 currVelocity;

    [Range(5, 500)]
    public float maxX = 5;
    [Range(5, 500)]
    public float maxY = 5;
    [Range(5, 500)]
    public float maxZ = 5;
    [Range(0.1f, 1)]
    public float inputMultiplier = 0.5f;

    public bool boosted = false;
    public bool bogged = false;

	// Use this for initialization
	void Start ()
    {
        playerRigidBody = GetComponent<Rigidbody>();
	}

    void Update()
    {
        try
        {
            currVelocity = playerRigidBody.velocity;
        }
        catch(MissingReferenceException rigidbodyDestroyed) { }
    }

    void OnTriggerEnter(Collider other)
    {
        
        try
        {
            if (other.gameObject.name.Equals("Boost Panel"))
            {
                playerRigidBody.velocity = playerRigidBody.velocity * GetComponent<SpeedPanel>().boostFactor;
            }
            else if (other.gameObject.name.Equals("Bog Panel"))
            {
                playerRigidBody.velocity = playerRigidBody.velocity * GetComponent<SpeedPanel>().bogFactor;
            }
            else if (other.gameObject.name.Equals("Neutralizer"))
            {
                if(boosted)
                {
                    playerRigidBody.velocity = playerRigidBody.velocity * GetComponent<SpeedPanel>().bogFactor;
                }
                else if(bogged)
                {
                    playerRigidBody.velocity = playerRigidBody.velocity * GetComponent<SpeedPanel>().boostFactor;
                }
            }
        }
        catch(MissingComponentException playerRigidBodyDestroyed) { }
    }

	// Update is called once per frame
	void FixedUpdate ()
    {
        // Capture input as accelerometer + keyboard input
        float inputH = Mathf.Clamp(Input.acceleration.x + (Input.GetAxis("Horizontal") * inputMultiplier), -1.002f, 1.002f);
        float inputV = Mathf.Clamp(Input.acceleration.y + (Input.GetAxis("Vertical") * inputMultiplier), -1.002f, 1.002f);        

        Vector3 totalMovement = Vector3.zero;

        try
        {
            if (boosted)
            {
                totalMovement = ((Vector3.forward * inputV * forwardSpeed) + (Vector3.right * inputH * forwardSpeed)) * GetComponent<SpeedPanel>().boostFactor;
            }
            else if (bogged)
            {
                totalMovement = ((Vector3.forward * inputV * forwardSpeed) + (Vector3.right * inputH * forwardSpeed)) * GetComponent<SpeedPanel>().bogFactor;
            }
            else
            {
                totalMovement = (Vector3.forward * inputV * forwardSpeed) + (Vector3.right * inputH * forwardSpeed);
            }

            // Apply input to the ball
            playerRigidBody.AddForce(totalMovement);

            // Cap max velocity on the ball
            Vector3 currentVelocity = playerRigidBody.velocity;

            currentVelocity = playerRigidBody.velocity;
            currentVelocity.x = Mathf.Clamp(currentVelocity.x, -maxX, maxX);
            currentVelocity.y = Mathf.Clamp(currentVelocity.y, -maxY, maxY);
            currentVelocity.z = Mathf.Clamp(currentVelocity.z, -maxZ, maxZ);

            playerRigidBody.velocity = currentVelocity;
        }
        catch(MissingReferenceException playerRigidBodyDestroyed) { }
	}
}
