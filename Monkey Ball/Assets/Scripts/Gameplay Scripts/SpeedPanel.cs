﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPanel : MonoBehaviour 
{
    public float boostTime = 5.0f;
    [Range(1.1f, 3.0f)]
    public float boostFactor = 1.5f;
    [Range(1.1f, 3.0f)]
    public float bogFactor = 0.5f;
    [Range(2, 8)]
    public float jumpForce = 50.0f;

    [HideInInspector]
    public float initX, initY, initZ;

    void Start()
    {
        initX = GetComponent<PushBall>().maxX;
        initY = GetComponent<PushBall>().maxY;
        initZ = GetComponent<PushBall>().maxZ;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Boost Panel"))
        {
            Debug.Log("YOU GOT BOOST POWER!!!");
            StartCoroutine(Boost());
        }
        else if (other.gameObject.tag.Equals("Bog Panel"))
        {
            Debug.Log("YOU'VE HIT A TRAP!!!");
            StartCoroutine(Bog());
        }
        else if (other.gameObject.tag.Equals("Jump Panel"))
        {
            Debug.Log("Boing");
            Jump();
        }
        else if (other.gameObject.tag.Equals("Neutralizer"))
        {
            Debug.Log("Canceling out all panel effects");
            Neutralize();
        }
    }

    IEnumerator Boost()
    {
        if (!GetComponent<PushBall>().boosted)
        {
            GetComponent<PushBall>().boosted = true;
            GetComponent<PushBall>().bogged = false;
            Debug.Log("Current maximums\n" + GetComponent<PushBall>().maxX + "\n" + GetComponent<PushBall>().maxY + "\n" + GetComponent<PushBall>().maxZ);
            GetComponent<PushBall>().maxX = boostFactor * initX;
            GetComponent<PushBall>().maxY = boostFactor * initY;
            GetComponent<PushBall>().maxZ = boostFactor * initZ;            
            Debug.Log("After hitting plate\n" + GetComponent<PushBall>().maxX + "\n" + GetComponent<PushBall>().maxY + "\n" + GetComponent<PushBall>().maxZ);
            
        }

        float timeStep = 0;
        while (timeStep < boostTime)
        {
            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        Debug.Log("Boost gone");
        GetComponent<PushBall>().maxX = initX;
        GetComponent<PushBall>().maxY = initY;
        GetComponent<PushBall>().maxZ = initZ;
        Debug.Log("Boost wearout maximums\n" + GetComponent<PushBall>().maxX + "\n" + GetComponent<PushBall>().maxY + "\n" + GetComponent<PushBall>().maxZ);
        
    }

    IEnumerator Bog()
    {
        if (!GetComponent<PushBall>().bogged)
        {
            GetComponent<PushBall>().boosted = false;
            GetComponent<PushBall>().bogged = true;
            Debug.Log("Current maximums\n" + GetComponent<PushBall>().maxX + "\n" + GetComponent<PushBall>().maxY + "\n" + GetComponent<PushBall>().maxZ);
            GetComponent<PushBall>().maxX = initX / bogFactor;
            GetComponent<PushBall>().maxY = initY / bogFactor;
            GetComponent<PushBall>().maxZ = initZ / bogFactor;            
            Debug.Log("After hitting plate\n" + GetComponent<PushBall>().maxX + "\n" + GetComponent<PushBall>().maxY + "\n" + GetComponent<PushBall>().maxZ);
            
        }

        float timeStep = 0;
        while (timeStep < boostTime)
        {

            timeStep += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        Debug.Log("Bog gone");
        GetComponent<PushBall>().maxX = initX;
        GetComponent<PushBall>().maxY = initY;
        GetComponent<PushBall>().maxZ = initZ;
        Debug.Log("Bog wearout maximums\n" + GetComponent<PushBall>().maxX + "\n" + GetComponent<PushBall>().maxY + "\n" + GetComponent<PushBall>().maxZ);
    }

    void Neutralize()
    {
        if(GetComponent<PushBall>().boosted)
        {
            GetComponent<PushBall>().boosted = false;
            GetComponent<PushBall>().maxX = initX;
            GetComponent<PushBall>().maxY = initY;
            GetComponent<PushBall>().maxZ = initZ;
        }
        else if (GetComponent<PushBall>().bogged)
        {
            GetComponent<PushBall>().bogged = false;
            GetComponent<PushBall>().maxX = initX;
            GetComponent<PushBall>().maxY = initY;
            GetComponent<PushBall>().maxZ = initZ;
        }
    }

    void Jump()
    {
        Rigidbody playerBody = GetComponent<Rigidbody>();

        playerBody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
}
