﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{    
    public int stageTime;
    public Text timerText;
    public AudioClip timeUp;

    Vector3 initialPlayerPos;
    Vector3 initialCameraPos;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("SubtractTime", 1, 1);
        timerText.text = stageTime.ToString();
        initialPlayerPos = GameObject.Find("Player").transform.position;
        initialCameraPos = GameObject.Find("Main Camera").transform.position;
	}

    void SubtractTime()
    {
        int curTime = int.Parse(timerText.text) - 1;
        timerText.text = curTime.ToString();

        if(curTime <= 0)
        {
            GameObject.Find("Director").GetComponent<Director>().Timeout();
            StopTimer();
            GameObject.Find("Main Camera").GetComponent<AudioSource>().PlayOneShot(timeUp);
            Invoke("Reload", 1.59f);
            GameObject.Find("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        }
    }

    void Reload()
    {
        GameObject.Find("Player").transform.position = initialPlayerPos;
        GameObject.Find("Main Camera").transform.position = initialCameraPos;
        GameObject.Find("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        GameObject.Find("CountText").GetComponent<Text>().text = "0";
        ResetTimer();        
    }

    public void StopTimer()
    {
        CancelInvoke();
    }

    public void ResetTimer()
    {
        timerText.text = stageTime.ToString();
        InvokeRepeating("SubtractTime", 1, 1);
    }
}
