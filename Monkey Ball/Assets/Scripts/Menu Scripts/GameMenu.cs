﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GameMenuStates
{
    PAUSE_MENU,
    MENU_CONFIRM_RESET,
    MENU_CONFIRM_END,
    IN_GAME
}

public enum GameMenuCommands
{
    PAUSE_GAME,
    GOTO_RESET_CONFIRM,
    GOTO_MAIN_MENU_CONFIRM,
    RETURN_TO_GAME
}


public class GameMenu : MonoBehaviour 
{
    Dictionary<GameMenuTransitions, GameMenuStates> menuTransitions;
    Dictionary<string, GameMenuCommands> enumParse;

    public GameMenuStates CurrentState { get; private set; }
    public GameMenuStates PreviousState { get; private set; }

    Vector3 initialPlayerPos;
    Vector3 initialCameraPos;

    [SerializeField] GameObject pauseButton;
    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject confirmResetMenu;
    [SerializeField] GameObject confirmEndMenu;

    void Start()
    {
        pauseButton.SetActive(true);
        pauseMenu.SetActive(false);
        confirmResetMenu.SetActive(false);
        confirmEndMenu.SetActive(false);
        initialPlayerPos = GameObject.Find("Player").transform.position;
        initialCameraPos = GameObject.Find("Main Camera").transform.position;

    //    CurrentState = GameMenuStates.IN_GAME;

    //    menuTransitions = new Dictionary<GameMenuTransitions, GameMenuStates>
    //    {
    //        // Can only go from the game to the pause menu
    //        {new GameMenuTransitions(GameMenuStates.IN_GAME, GameMenuCommands.PAUSE_GAME), GameMenuStates.PAUSE_MENU},
    //        // Can access the confirmation menu for resetting the level or returning to the main menu from the pause menu or return to the game
    //        {new GameMenuTransitions(GameMenuStates.PAUSE_MENU, GameMenuCommands.RETURN_TO_GAME), GameMenuStates.IN_GAME},
    //        {new GameMenuTransitions(GameMenuStates.PAUSE_MENU, GameMenuCommands.GOTO_RESET_CONFIRM), GameMenuStates.MENU_CONFIRM_RESET},
    //        {new GameMenuTransitions(GameMenuStates.PAUSE_MENU, GameMenuCommands.GOTO_MAIN_MENU_CONFIRM), GameMenuStates.MENU_CONFIRM_END},            
    //        // Can return to the pause menu from the reset confirm menu
    //        {new GameMenuTransitions(GameMenuStates.MENU_CONFIRM_RESET, GameMenuCommands.PAUSE_GAME), GameMenuStates.PAUSE_MENU},
    //        // Can return to the pause menu from the end game confirm menu
    //        {new GameMenuTransitions(GameMenuStates.MENU_CONFIRM_END, GameMenuCommands.PAUSE_GAME), GameMenuStates.PAUSE_MENU}            
    //    };

    //    enumParse = new Dictionary<string, GameMenuCommands>
    //    {
    //        {"pause game", GameMenuCommands.PAUSE_GAME},
    //        {"unpause game", GameMenuCommands.RETURN_TO_GAME},
    //        {"goto reset confirm", GameMenuCommands.GOTO_RESET_CONFIRM},
    //        {"goto end game confirm", GameMenuCommands.GOTO_MAIN_MENU_CONFIRM}            
    //    };
    }

    //public void MoveNextAndTransition(string command)
    //{
    //    PreviousState = CurrentState;
    //    GameMenuCommands newCommand;
    //    if (!enumParse.TryGetValue(command, out newCommand))
    //        throw new UnityException("Invalid command " + command);
    //    CurrentState = GetNext(newCommand);
    //    Transition();
    //}

    //GameMenuStates GetNext(GameMenuCommands command)
    //{
    //    GameMenuTransitions newTransition = new GameMenuTransitions(CurrentState, command);
    //    GameMenuStates newState;

    //    if(!menuTransitions.TryGetValue(newTransition, out newState))
    //        throw new UnityException("Invalid transition: " + CurrentState + " -> " + command);

    //    return newState;
    //}    

    //void Transition()
    //{
    //    switch (PreviousState)
    //    {
    //        case(GameMenuStates.IN_GAME):
    //            if (CurrentState == GameMenuStates.PAUSE_MENU)
    //            {
    //                Time.timeScale = 0;
    //                pauseButton.SetActive(false);
    //                pauseMenu.SetActive(true);
    //            }
    //            break;
    //        case(GameMenuStates.PAUSE_MENU):
    //            if (CurrentState == GameMenuStates.MENU_CONFIRM_RESET)
    //            {
    //                pauseMenu.SetActive(false);
    //                confirmResetMenu.SetActive(true);
    //            }
    //            else if (CurrentState == GameMenuStates.MENU_CONFIRM_END)
    //            {
    //                pauseMenu.SetActive(false);
    //                confirmEndMenu.SetActive(true);
    //            }
    //            else if (CurrentState == GameMenuStates.IN_GAME)
    //            {
    //                Time.timeScale = 1;
    //                pauseMenu.SetActive(false);
    //                pauseButton.SetActive(true);
    //            }
    //            break;
    //        case(GameMenuStates.MENU_CONFIRM_RESET):
    //            if (CurrentState == GameMenuStates.PAUSE_MENU)
    //            {
    //                confirmResetMenu.SetActive(false);
    //                pauseMenu.SetActive(true);
    //            }
    //            break;
    //        case (GameMenuStates.MENU_CONFIRM_END):
    //            if (CurrentState == GameMenuStates.PAUSE_MENU)
    //            {
    //                confirmEndMenu.SetActive(false);
    //                pauseMenu.SetActive(true);
    //            }
    //            break;
    //    }
    //}

    public void PauseButton()
    {
        Time.timeScale = 0;
        pauseButton.SetActive(false);
        pauseMenu.SetActive(true);
    }

    public void ResumeButton()
    {
        Time.timeScale = 1;
        pauseButton.SetActive(true);
        pauseMenu.SetActive(false);
    }

    public void ResetButton()
    {
        pauseMenu.SetActive(false);
        confirmResetMenu.SetActive(true);
    }

    public void EndGameButton()
    {
        pauseMenu.SetActive(false);
        confirmEndMenu.SetActive(true);
    }

    public void CancelSelectionButton(bool fromReset)
    {
        if(fromReset)
            confirmResetMenu.SetActive(false);
        else
            confirmEndMenu.SetActive(false);
        pauseMenu.SetActive(true);
    }

    public void ConfirmResetButton()
    {
        Time.timeScale = 1;
        confirmResetMenu.SetActive(false);
        GameObject.Find("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        GameObject.Find("Main Camera").GetComponent<Timer>().StopTimer();
        HidePause();
        GameObject.Find("Director").GetComponent<Director>().Reset();
        GameObject.Find("Main Camera").GetComponent<Timer>().ResetTimer();
        GameObject.Find("CountText").GetComponent<Text>().text = "0";
        Invoke("ResetLevel", 1.59f);        
    }

    public void ConfirmEndGameButton()
    {
        Time.timeScale = 1;
        confirmEndMenu.SetActive(false);
        GameObject.Find("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        GameObject.Find("Main Camera").GetComponent<Timer>().StopTimer();
        HidePause();
        Invoke("EndGame", 1.59f);
        
    }

    void ResetLevel()
    {
        pauseButton.SetActive(true);
        GameObject.Find("Player").transform.position = initialPlayerPos;
        GameObject.Find("Main Camera").transform.position = initialCameraPos;
        GameObject.Find("Player").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
    }

    void EndGame()
    {
        SceneManager.LoadScene(1);
    }

    public void HidePause()
    {
        pauseButton.SetActive(false);
    }
}
