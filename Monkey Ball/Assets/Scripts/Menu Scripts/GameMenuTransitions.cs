﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenuTransitions : MonoBehaviour 
{
    GameMenuStates currentGameState;
    GameMenuCommands gameMenuCommand;

    public GameMenuTransitions(GameMenuStates thisState, GameMenuCommands thisCommand)
    {
        currentGameState = thisState;
        gameMenuCommand = thisCommand;
    }

    public override int GetHashCode()
    {
        return 17 + 31 * currentGameState.GetHashCode() + 31 * gameMenuCommand.GetHashCode();
    }

    public override bool Equals(object obj)
    {
        GameMenuTransitions other = obj as GameMenuTransitions;
        return other != null && this.currentGameState == other.currentGameState && this.gameMenuCommand == other.gameMenuCommand;
    }
}
