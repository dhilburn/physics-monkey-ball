﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum MenuStates
{
    MENU_MAIN,
    MENU_LEVEL_SELECT,
    MENU_HIGHSCORES_TABLE,
    MENU_CREDITS,
    MENU_QUIT
}

public enum MenuCommands
{    
    GOTO_LEVEL_SELECT,
    GOTO_HIGHSCORES,
    GOTO_CREDITS,
    GOTO_MAIN,
    QUIT_APP
}

public class MainMenu : MonoBehaviour
{
    Dictionary<MainMenuTransitions, MenuStates> menuTransitions;
    Dictionary<string, MenuCommands> enumParse;

    public MenuStates CurrentState { get; private set; }
    public MenuStates PreviousState { get; private set; }

    public GameObject mainMenu;
    public GameObject levelSelectMenu;
    public GameObject creditsMenu;       
    public GameObject highScoreMenu;
    public GameObject analyticsMenu;

    int analyticCode;

    void Start()
    {
        mainMenu.SetActive(true);
        levelSelectMenu.SetActive(false);
        creditsMenu.SetActive(false);
        highScoreMenu.SetActive(false);
        analyticsMenu.SetActive(false); 
        analyticCode = 0;
    }

        #region Old Menu Code
        //CurrentState = MenuStates.MENU_MAIN;

        //menuTransitions = new Dictionary<MainMenuTransitions, MenuStates>
        //{
        //    {new MainMenuTransitions(MenuStates.MENU_MAIN, MenuCommands.GOTO_LEVEL_SELECT), MenuStates.MENU_LEVEL_SELECT },
        //    {new MainMenuTransitions(MenuStates.MENU_MAIN, MenuCommands.GOTO_HIGHSCORES), MenuStates.MENU_HIGHSCORES_TABLE },
        //    {new MainMenuTransitions(MenuStates.MENU_MAIN, MenuCommands.GOTO_CREDITS), MenuStates.MENU_CREDITS },
        //    {new MainMenuTransitions(MenuStates.MENU_LEVEL_SELECT, MenuCommands.GOTO_MAIN), MenuStates.MENU_MAIN },
        //    {new MainMenuTransitions(MenuStates.MENU_CREDITS, MenuCommands.GOTO_MAIN), MenuStates.MENU_MAIN },
        //    {new MainMenuTransitions(MenuStates.MENU_HIGHSCORES_TABLE, MenuCommands.GOTO_MAIN), MenuStates.MENU_MAIN },
        //    {new MainMenuTransitions(MenuStates.MENU_MAIN, MenuCommands.QUIT_APP), MenuStates.MENU_QUIT }
        //};

        //enumParse = new Dictionary<string, MenuCommands>
        //{
        //    {"goto level select", MenuCommands.GOTO_LEVEL_SELECT },
        //    {"goto highscores", MenuCommands.GOTO_HIGHSCORES },
        //    {"goto credits", MenuCommands.GOTO_CREDITS },
        //    {"goto main", MenuCommands.GOTO_MAIN },
        //    {"quit app", MenuCommands.QUIT_APP }
        //};
    //}

    //public void MoveNextAndTransition(string command)
    //{
    //    PreviousState = CurrentState;
    //    MenuCommands newCommand;
    //    if (!enumParse.TryGetValue(command, out newCommand))
    //        throw new UnityException("Invalid command " + command);
    //    CurrentState = GetNext(newCommand);
    //    Transition();
    //}

    //MenuStates GetNext(MenuCommands command)
    //{
    //    MainMenuTransitions newTransition = new MainMenuTransitions(CurrentState, command);
    //    MenuStates newState;

    //    if (!menuTransitions.TryGetValue(newTransition, out newState))
    //        throw new UnityException("Invalid transition: " + CurrentState + " -> " + command);

    //    return newState;
    //}    

    //void Transition()
    //{
    //    switch(PreviousState)
    //    {
    //        #region Main Menu Transitions
    //        case (MenuStates.MENU_MAIN):
    //            if(CurrentState == MenuStates.MENU_LEVEL_SELECT)
    //            {
    //                mainMenu.SetActive(false);
    //                levelSelectMenu.SetActive(true);
    //            }
    //            else if (CurrentState == MenuStates.MENU_HIGHSCORES_TABLE)
    //            {
    //                mainMenu.SetActive(false);
    //                highScoreMenu.SetActive(true);
    //            }
    //            else if(CurrentState == MenuStates.MENU_CREDITS)
    //            {
    //                mainMenu.SetActive(false);
    //                creditsMenu.SetActive(true);
    //            }
    //            else if(CurrentState == MenuStates.MENU_QUIT)
    //            {
    //                Application.Quit();
    //            }
    //            break;
    //        #endregion
    //        case (MenuStates.MENU_LEVEL_SELECT):
    //            if(CurrentState == MenuStates.MENU_MAIN)
    //            {
    //                levelSelectMenu.SetActive(false);
    //                mainMenu.SetActive(true);
    //            }
    //            break;
    //        case (MenuStates.MENU_HIGHSCORES_TABLE):
    //            if(CurrentState == MenuStates.MENU_MAIN)
    //            {
    //                highScoreMenu.SetActive(false);
    //                mainMenu.SetActive(true);
    //            }
    //            break;
    //        case (MenuStates.MENU_CREDITS):
    //            if (CurrentState == MenuStates.MENU_MAIN)
    //            {
    //                creditsMenu.SetActive(false);
    //                mainMenu.SetActive(true);
    //            }
    //            break;
    //    }
    //}
        #endregion

    public void StartGameButton()
    {
        mainMenu.SetActive(false);
        levelSelectMenu.SetActive(true);
    }

    public void CreditsButton()
    {
        mainMenu.SetActive(false);
        creditsMenu.SetActive(true);
    }

    public void HighScoresButton()
    {
        mainMenu.SetActive(false);
        highScoreMenu.SetActive(true);
    }

    public void Analytics()
    {
        analyticCode++;
        if (analyticCode >= 5)
        {
            creditsMenu.SetActive(false);
            analyticsMenu.SetActive(true);
        }
    }

    public void BackButton(int index)
    {
        switch (index)
        {
            case 0:
                levelSelectMenu.SetActive(false);
                break;
            case 1:
                highScoreMenu.SetActive(false);
                break;
            case 2:
                creditsMenu.SetActive(false);
                break;
        }
        mainMenu.SetActive(true);
    }

    public void LeaveAnalytics()
    {
        analyticsMenu.SetActive(false);
        mainMenu.SetActive(true);
        analyticCode = 0;
    }

    public void QuitGame()
    {
        //GameObject.Find("Director").GetComponent<Analytics>().WriteAnalyticsFile();
        Application.Quit();
    }

    public void LoadLevel1()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadLevel2()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }

    public void LoadLevel3()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 3);
    }
}
