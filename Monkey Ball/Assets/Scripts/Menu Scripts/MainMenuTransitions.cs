﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuTransitions : MonoBehaviour 
{
	MenuStates currentMenuState;
    MenuCommands mainMenuCommand;

    public MainMenuTransitions(MenuStates thisState, MenuCommands thisCommand)
    {
        currentMenuState = thisState;
        mainMenuCommand = thisCommand;
    }

    public override int GetHashCode()
    {
        return 17 + 31 * currentMenuState.GetHashCode() + 31 * mainMenuCommand.GetHashCode();
    }

    public override bool Equals(object obj)
    {
        MainMenuTransitions other = obj as MainMenuTransitions;
        return other != null && this.currentMenuState == other.currentMenuState && this.mainMenuCommand == other.mainMenuCommand;
    }
}
